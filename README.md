# THIS PROJECT HAS MOVED TO GITHUB: https://github.com/mankoff/PROMICE-AWS-data-issues

# PROMICE AWS Data Issue

Automatic weather station (AWS) data from the Greenland Ice Sheet are often imperfect due to the complexity and conditions involved in installing and maintaining the AWS.

This goal of this project is to provide a public space to document and solve issues with PROMICE AWS data.

# Issue Labels

+ Time: ~ALL_TIME ~"2012" ~"2013" ~"2014" ~"2015" ~"2016" ~"2017" ~"2018"
+ Station: ~ALL_STATION ~UPE_L ~UPE_U 
+ Sensor: ~"Wind Direction" ~"SR50 - boom" ~"SR50 - stakes"


# Workflow

+  If you are using PROMICE AWS data and something seems amiss, you can search the [issues](https://gitlab.com/mankoff/PROMICE-AWS-data-issues/issues) to see if someone else has documented the issue.
  + Issues can be searched using [labels](https://gitlab.com/mankoff/PROMICE-AWS-data-issues/labels), and filtered by Station Name, Year, or Sensor
  + Click in the search box to add additional label filters
  
+ Once an issue has been submitted, a PROMICE scientist will examine and add the ~CONFIRMED label.

+ If an issue can be solved, a solution will be added to the issue and the ~SOLUTION label will be added.
